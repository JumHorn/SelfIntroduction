// pages/index/blog.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    listData: [
      { "info": "姓名", "text": "黄俊" },
      { "info": "性别", "text": "男" },
      { "info": "手机", "text": "18750201580" },
      { "info": "邮箱", "text": "JumHorn@gmail.com" }
    ],
    ability:[
      {"tech": "C/C++编程语言" },
      {"tech": "STL标准库/Boost准标准库" },
      {"tech": "Qt cross platform"}
    ],
    major:[
      { "course": "1.数据结构和算法" },
      { "course": "2.计算机网络" },
      { "course": "3.模式识别" },
      {"course":"4.人工智能"},
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})